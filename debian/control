Source: chuck
Maintainer: Paul Brossier <piem@debian.org>
Uploaders: Barak A. Pearlmutter <bap@debian.org>
Homepage: https://chuck.cs.princeton.edu
Standards-Version: 4.7.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/chuck.git
Vcs-Browser: https://salsa.debian.org/debian/chuck
Build-Depends: debhelper-compat (= 13),
    bison,
    docbook-to-man,
    flex,
    libasound2-dev,
    libjack-dev,
    libpulse-dev,
    libsndfile1-dev,
    python3,
    jdupes,
Priority: optional
Section: sound

Package: chuck
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
    chuck-data,
Suggests:
    jackd,
Description: Concurrent, On-the-fly Audio Programming Language
 ChucK is a new audio programming language for real-time synthesis,
 composition, and performance, which runs on commodity operating systems.
 .
 ChucK presents a new time-based concurrent programming model, which supports
 multiple, simultaneous, dynamic control rates, and the ability to add, remove,
 and modify code, on-the-fly, while the program is running, without stopping or
 restarting.

Package: chuck-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: Examples and data files for ChucK
 This package provides the examples, documentation and other
 architecture-independent files for ChucK, the concurrent, on-the-fly
 audio programming language.
